import java.util.*;

public class BackTrack 
{
    protected Maze maze;

   
    /**
     * Initializes this BackTrack object from an application.
     * 
     * @param maze the application
     */    
     public BackTrack (Maze maze)
     {
         this.maze = maze;
     } // constructor


     /**
       * Attempts to reach a goal through a given position.
       * 
       * @param pos the given position.
       * 
       * @return true if the attempt succeeds; otherwise, false.
       */        
     public boolean tryToReachGoal (Position pos)
     {                
         Iterator<Position> itr = maze.iterator (pos);

         while (itr.hasNext())
         {             
              pos = itr.next();               
              if (maze.isOK (pos))
              {
                  maze.markAsPossible (pos);
                  if (maze.isGoal (pos) || tryToReachGoal (pos))
                       return true;                       
                  maze.markAsDeadEnd (pos);
              } // pos may be on a path to a goal
         } // while         
         return false;
    } // method tryToReachGoal
    
} // class BackTrack
