/**
 * Custom LinkedList Implementation
 *
 * @param <T>
 */
public class CustomLinkedList<T> {

    private T element;

    private CustomLinkedList<T> next;

    public CustomLinkedList(T element, CustomLinkedList<T> next) {
        this.element = element;
        this.next = next;
    }

    public T getElement() {
        return element;
    }

    public CustomLinkedList<T> getNext() {
        return next;
    }

    public void setNext(CustomLinkedList<T> next) {
        this.next = next;
    }
}
