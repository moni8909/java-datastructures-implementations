import java.util.*;

/**
 * Graph implementaion represented as an ajacency matrix
 */
public class Graph {

    public Node rootNode;
    public List<Node> nodes = new ArrayList();
    public int[][] adjacencyMatrix;
    int size;

    public Node getRootNode() {
        return this.rootNode;
    }

    public void setRootNode(Node node) {
        this.rootNode = node;
    }

    public void addNode(Node node) {
        nodes.add(node);
    }

    /**
     * Connects two nodes to each other, a square in the
     *
     * @param start
     * @param end
     */
    public void connectTwoNodes(Node start, Node end) {
        if (adjacencyMatrix == null) {
            size = nodes.size();
            adjacencyMatrix = new int[size][size];
        }

        int startIndex = nodes.indexOf(start);
        int endIndex = nodes.indexOf(end);

        adjacencyMatrix[startIndex][endIndex] = 1;
        adjacencyMatrix[endIndex][startIndex] = 1;
    }

    /**
     * Returns an unvisited child node for a given parent node
     *
     * @param parentNode
     * @return
     */
    private Node getUnvisitedChildNode(Node parentNode) {
        int index = nodes.indexOf(parentNode);

        int i = 0;

        while (i < size) {
            if (adjacencyMatrix[index][i] == 1 && !nodes.get(i).visited) {
                return nodes.get(i);
            }
            i++;
        }

        return null;
    }

    /**
     * A breadth first search implementation
     * 1. Traversal starts with the root node
     * 2. Root node is added to the list of visited nodes
     * 3. It is then marked as visited
     * 4. Loop over the queue until empty
     * 4a. Remove node from queue once visited
     * 4b. Get the child node of the node in question (unvisited)
     * 4c. While that child node is not null
     * 4d. Add child node to list of visited nodes
     */
    public void breadthFirstSearch() {
        Queue<Node> visitedNodes = new LinkedList();

        visitedNodes.add(this.rootNode);

        printNode(this.rootNode);

        rootNode.visited = true;

        while (!visitedNodes.isEmpty()) {
            Node node = visitedNodes.remove();
            Node child = getUnvisitedChildNode(node);

            while (child != null) {
                child.visited = true;
                printNode(child);
                visitedNodes.add(child);
            }
        }
        clearVisitedNodes();
    }

    /**
     * A depth first search implementation
     */
    public void depthFirstSearch() {
        Stack<Node> visitedNodes = new Stack();

        visitedNodes.push(this.rootNode);

        rootNode.visited = true;

        printNode(rootNode);

        while (!visitedNodes.isEmpty()) {
            Node node = visitedNodes.peek();
            Node child = getUnvisitedChildNode(node);

            if (child != null) {
                child.visited = true;
                printNode(child);
                visitedNodes.push(child);
            } else {
                visitedNodes.pop();
            }
        }
        clearVisitedNodes();
    }

    private void clearVisitedNodes() {
        int i = 0;
        while (i < size) {
            Node node = nodes.get(i);
            node.visited = false;
            i++;
        }
    }

    private void printNode(Node n) {
        System.out.print(n.label + " ");
    }
}
