import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An example of a lexer - essentially a tokenizer that interprets a string
 * Let's say we have the following expression 11 + 22 - 33
 * The expected output should be
 * (NUMBER 11)
 * (BINARYOP +)
 * (NUMBER 22)
 * (BINARYOP -)
 * (NUMBER 33)
 */
public class Lexer {
    /**
     * For a given list of regex patterns, compile those into a pattern via a string builder
     * Then using a <code>Matcher</code> object, perform the matching for the given input
     *
     * @param input
     * @return an ArrayDeque consisting of listOfTokens
     */
    public static List<Token> analyseExpression(String input) {
        List<Token> listOfTokens = new ArrayList<>();

        StringBuilder stringBuilder = new StringBuilder();

        for (TokenType tokenType : TokenType.values()) {
            stringBuilder.append(String.format("|(?<%s>%s)", tokenType.name(), tokenType.pattern));
        }

        Pattern tokenPatterns = Pattern.compile(stringBuilder.substring(1));

        Matcher matcher = tokenPatterns.matcher(input);

        //While the matcher is busy searching
        while (matcher.find()) {
            if (matcher.group(TokenType.NUMBER.name()) != null) {
                listOfTokens.add(new Token(TokenType.NUMBER, matcher.group(TokenType.NUMBER.name())));
                continue;
            } else if (matcher.group(TokenType.BINARYOPERATION.name()) != null) {
                listOfTokens.add(new Token(TokenType.BINARYOPERATION, matcher.group(TokenType.BINARYOPERATION.name())));
                continue;
            } else if (matcher.group(TokenType.WHITESPACE.name()) != null)
                continue;
        }

        return listOfTokens;
    }

    /**
     * An enum to hold the tokenizer definitions
     */
    enum TokenType {
        NUMBER("-?[0-9]+"),
        BINARYOPERATION("[*|/|+|-]"),
        WHITESPACE("[ \t\f\r\n]+"),
        BRACKETS("");

        public final String pattern;

        TokenType(String pattern) {
            this.pattern = pattern;
        }
    }

    /**
     * Token class to model a token
     */
    public static class Token {
        private TokenType type;
        private String data;

        public Token(TokenType type, String data) {
            this.type = type;
            this.data = data;
        }

        public TokenType getType() {
            return type;
        }

        public void setType(TokenType type) {
            this.type = type;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return String.format("(%s %s)", type.name(), data);
        }
    }
}
