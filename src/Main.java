public class Main {

    public static void main(String[] args) {

        /*Node nA = new Node('A');
        Node nB = new Node('B');
        Node nC = new Node('C');
        Node nD = new Node('D');
        Node nE = new Node('E');
        Node nF = new Node('F');

        Graph graph = new Graph();
        graph.addNode(nA);
        graph.addNode(nB);
        graph.addNode(nC);
        graph.addNode(nD);
        graph.addNode(nE);
        graph.addNode(nF);
        graph.setRootNode(nA);

        graph.connectTwoNodes(nA, nB);
        graph.connectTwoNodes(nA, nC);
        graph.connectTwoNodes(nA, nD);

        graph.connectTwoNodes(nB, nE);
        graph.connectTwoNodes(nB, nF);
        graph.connectTwoNodes(nC, nF);

        System.out.println("DFS Traversal of a tree is ------------->");
        graph.depthFirstSearch();

        System.out.println("\nBFS Traversal of a tree is ------------->");
        graph.breadthFirstSearch();*/

        System.out.println(evaluateExpression("2+2*(3+3)"));
    }

    /**
     * Reversing a linked list in place
     *
     * @param original
     * @param <T>
     * @return
     */
    public static <T> CustomLinkedList<T> reverse(final CustomLinkedList<T> original) {

        if (original == null) {
            throw new NullPointerException("Cannot reverse a null list");
        }

        if (original.getNext() == null) {
            return original;
        }

        final CustomLinkedList<T> next = original.getNext();
        original.setNext(null);

        final CustomLinkedList<T> othersReversed = reverse(next);

        next.setNext(original);
        return othersReversed;
    }

    public static double evaluateExpression(final String expression) {
        class Parser {
            int positionOfCharacter = -1, character;

            void positionCharacter() {
                if (++positionOfCharacter < expression.length())
                    character = expression.charAt(positionOfCharacter);
                else
                    character = -1;
            }

            void eatSpace() {
                while (Character.isWhitespace(character)) {
                    positionCharacter();
                }
            }

            double parse() {
                positionCharacter();
                double v = parseExpression();
                if (character != -1) throw new RuntimeException("Unexpected: " + (char) character);
                return v;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor | term brackets
            // factor = brackets | number | factor `^` factor
            // brackets = `(` expression `)`

            double parseExpression() {
                double v = parseTerm();
                while (true) {
                    eatSpace();
                    if (character == '+') { // addition
                        positionCharacter();
                        v += parseTerm();
                    } else if (character == '-') { // subtraction
                        positionCharacter();
                        v -= parseTerm();
                    } else {
                        return v;
                    }
                }
            }

            double parseTerm() {
                double v = parseFactor();
                while (true) {
                    eatSpace();
                    if (character == '/') { // division
                        positionCharacter();
                        v /= parseFactor();
                    } else if (character == '*' || character == '(') { // multiplication
                        if (character == '*') positionCharacter();
                        v *= parseFactor();
                    } else {
                        return v;
                    }
                }
            }

            double parseFactor() {
                double v;
                boolean negate = false;
                eatSpace();
                if (character == '+' || character == '-') { // unary plus & minus
                    negate = character == '-';
                    positionCharacter();
                    eatSpace();
                }
                if (character == '(') { // brackets
                    positionCharacter();
                    v = parseExpression();
                    if (character == ')') positionCharacter();
                } else { // numbers
                    StringBuilder sb = new StringBuilder();
                    while ((character >= '0' && character <= '9') || character == '.') {
                        sb.append((char) character);
                        positionCharacter();
                    }
                    if (sb.length() == 0) throw new RuntimeException("Unexpected: " + (char) character);
                    v = Double.parseDouble(sb.toString());
                }
                eatSpace();
                if (character == '^') { // exponentiation
                    positionCharacter();
                    v = Math.pow(v, parseFactor());
                }
                if (negate) v = -v; // unary minus is applied after exponentiation; e.g. -3^2=-9
                return v;
            }
        }
        return new Parser().parse();
    }
}

