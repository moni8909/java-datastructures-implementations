import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MaxFlow {

    @Test
    public void maxFlowTest() {
        FlowNetwork unit = new FlowNetwork(Arrays.asList(0, 1, 2, 3, 4, 5));

        unit.addEdge(0, 1, 16);
        unit.addEdge(0, 2, 13);
        unit.addEdge(1, 3, 12);
        unit.addEdge(1, 2, 10);
        unit.addEdge(2, 1, 4);
        unit.addEdge(2, 4, 14);
        unit.addEdge(3, 2, 9);
        unit.addEdge(4, 3, 7);
        unit.addEdge(3, 5, 20);
        unit.addEdge(4, 5, 4);

        assertThat(unit.maxFlow(0, 5), is(23)); //algorithm incorrectly returning 7
    }
}

class FlowNetwork {
    Map<Integer, List<Edge>> edges = new HashMap<>();
    Map<Edge, Integer> flow = new HashMap<>();

    public FlowNetwork(List<Integer> vertices) {
        vertices.forEach(vertex -> this.edges.put(vertex, new ArrayList<>()));
    }

    public Integer maxFlow(Integer source, Integer sink) {
        List<Edge> path;
        while (!(path = findAugmentedPath(source, sink, new ArrayList<>())).isEmpty()) {
            int flow = path.stream().mapToInt(e -> e.capacity - this.flow.get(e)).min().getAsInt();
            path.forEach(e -> {
                this.flow.put(e, this.flow.get(e) + flow);
                this.flow.put(e.reverseEdge, this.flow.get(e.reverseEdge) - flow);
            });
        }
        return edges.get(source).stream().mapToInt(e -> flow.get(e)).sum();
    }

    List<Edge> findAugmentedPath(Integer source, Integer sink, List<Edge> pathSoFar) {
        if (source == sink) {
            return pathSoFar;
        }
        for (Edge neighbourEdge : this.edges.get(source)) {
            int residual = neighbourEdge.capacity - flow.get(neighbourEdge);
            if (residual > 0 && !pathSoFar.contains(neighbourEdge)) {
                pathSoFar.add(neighbourEdge);
                List<Edge> path = findAugmentedPath(neighbourEdge.b, sink, pathSoFar);
                if (!path.isEmpty()) {
                    return path;
                }
            }
        }
        return Collections.emptyList();
    }


    public void addEdge(int source, int sink, int capacity) {
        Edge edge = new Edge(source, sink, capacity);
        Edge reverseEdge = new Edge(sink, source, 0);

        edge.reverseEdge = reverseEdge;

        reverseEdge.reverseEdge = edge;

        this.edges.get(source).add(edge);
        this.edges.get(sink).add(reverseEdge);

        flow.put(edge, 0);
        flow.put(reverseEdge, 0);
    }
}

class Edge {

    Integer capacity;
    Edge reverseEdge;
    int b;
    int a;

    public Edge(Integer a, Integer b, Integer capacity) {
        this.a = a;
        this.b = b;
        this.capacity = capacity;
    }
} 