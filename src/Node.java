/**
 * Class models the a graph node/vertex
 */
public class Node {
    public char label;
    public boolean visited = false;

    private Node reachableVertices[];

    public Node(char label) {
        this.reachableVertices = new Node[3];
        this.label = label;
    }

    public Node[] getReachableVertices() {
        return reachableVertices;
    }

    public void setReachableVertices(Node[] reachableVertices) {
        this.reachableVertices = reachableVertices;
    }
}
