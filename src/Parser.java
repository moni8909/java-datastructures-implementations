import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Manol on 10/01/2016.
 */
public class Parser {
    /**
     * Evaluates the expression passed as a string.
     *
     * @param input
     * @return result of the expression
     * @throws ArithmeticException
     */
    public static BigDecimal calculateExpressionValueWithMoreThanTwoNumbers(String input) throws ArithmeticException {
        List<Lexer.Token> listOfTokens = Lexer.analyseExpression(input);

        List<String> binaryOperationsInExpression = listOfTokens
                .stream()
                .filter(token -> token.getType().equals(Lexer.TokenType.BINARYOPERATION))
                .map(token -> token.getData())
                .collect(Collectors.toList());

        List<BigDecimal> listOfDigits = listOfTokens
                .stream()
                .filter(token -> token.getType().equals(Lexer.TokenType.NUMBER))
                .map(token -> BigDecimal.valueOf(Double.valueOf(token.getData())))
                .collect(Collectors.toList());

        BigDecimal total = BigDecimal.ZERO;
        int binaryOperationsCounter = 0;

        for (int i = 0; i <= listOfDigits.size(); i += 2) {

            outer:
            while (binaryOperationsCounter <= binaryOperationsInExpression.size()) {

                String binaryOperator = binaryOperationsInExpression.get(binaryOperationsCounter);

                if (!(i + 1 > listOfDigits.size())) {

                    BigDecimal numberOne = listOfDigits.get(i);
                    BigDecimal numberTwo = BigDecimal.ZERO;

                    //TODO: Need to get last digit in the expression - develop a method
                    if (i + 1 == listOfDigits.size()) {
                        if (numberTwo != numberOne) {
                            numberTwo = listOfDigits.get(listOfDigits.size() - 1); //get last digit
                        }
                    } else {
                        numberTwo = listOfDigits.get(i + 1); //otherwise get next
                    }

                    switch (binaryOperator) {
                        case "+":
                            total.add(numberOne).add(numberTwo);
                            break;
                        case "-":
                            total.subtract(numberOne).subtract(numberTwo);
                            break;
                        case "*":
                            if (total.equals(BigDecimal.ZERO))
                                total = numberOne.multiply(numberTwo);
                            else
                                total.multiply(numberOne).multiply(numberTwo);
                            break;
                    }

                    binaryOperationsCounter++;
                    break outer;
                }
            }
        }
        return total;
    }

    public static BigDecimal calculateExpressionValueForAsingleExpression(String input) {
        List<Lexer.Token> listOfTokens = Lexer.analyseExpression(input);

        String firstNumber = listOfTokens.get(0).getData();
        String secondNumber = listOfTokens.get(2).getData();
        BigDecimal total = BigDecimal.ZERO;
        String binaryOperator = listOfTokens.get(1).getData();

        switch (binaryOperator) {
            case "+":
                total = new BigDecimal(firstNumber).add(new BigDecimal(secondNumber));
                break;
            case "-":
                total = new BigDecimal(firstNumber).subtract(new BigDecimal(secondNumber));
                break;
            case "*":
                if (total.equals(BigDecimal.ZERO))
                    total = new BigDecimal(firstNumber).multiply(new BigDecimal(secondNumber));
                else
                    total = new BigDecimal(firstNumber).multiply(new BigDecimal(secondNumber));
                break;
            case "/":
                if (secondNumber.equals("0"))
                    throw new ArithmeticException("Cannot divide by zero");
                else
                    total = new BigDecimal(firstNumber).divide(new BigDecimal(secondNumber));
            default:
                return total;
        }
        return total;
    }
}
