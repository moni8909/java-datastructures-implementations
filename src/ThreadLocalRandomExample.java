import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ThreadLocalRandomExample {

    public static ThreadLocal<Random> random = new ThreadLocal<Random>() {
        @Override
        protected Random initialValue() {
            return new Random();
        }
    };

    public static void main(String[] args) throws InterruptedException {

        System.out.println("Regular thread-local Random");
        for (int i = 0; i < 3; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int c = 0; c < 5; c++) {
                        System.out.println(random.get().nextInt(10000));
                    }
                    System.out.println("========");
                }
            });
            t.start();
            t.join();
        }

        System.out.println("ThreadLocalRandom");
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int c = 0; c < 5; c++) {
                        System.out.println(ThreadLocalRandom.current().nextLong(10000));
                    }
                    System.out.println("========");
                }
            });
            t.start();
            t.join();
        }

    }
}

