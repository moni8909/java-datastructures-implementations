public class Tokenizer {
    int pos;
    char[] expression;

    Tokenizer(String expression) {
        this.expression = expression.toCharArray();
        this.pos = 0;
    }

    enum Type { OPERATOR, NUMBER, UNKNOWN }

    class Lexeme {
        String type, token;
        Lexeme(String type, String token) {
            this.type = type;
            this.token = token;
        }
    }

    Lexeme getToken() {
        StringBuilder token = new StringBuilder();
        boolean endOfToken = false;
        Type type = Type.UNKNOWN;
        while (!endOfToken && hasMoreToken())
        {
            while(expression[pos] == ' ')
                pos++;
            switch (expression[pos])
            {
                case '+':
                case '-':
                case '*':
                case '/':
                    if(type != Type.NUMBER)
                    {
                        type = Type.OPERATOR;
                        token.append(expression[pos]);
                        pos++;
                    }
                    endOfToken = true;
                    break;
                case ' ':
                    endOfToken = true;
                    pos++;
                    break;
                default:
                    if(Character.isDigit(expression[pos]) || expression[pos] == '.')
                    {
                        token.append(expression[pos]);
                        type = Type.NUMBER;
                    }
                    else
                    {
                        System.out.println("Systax error at position: " + pos);
                    }
                    pos++;
                    break;
            }
        }
        return new Lexeme(type.name().toLowerCase(), token.toString());
    }

    boolean hasMoreToken() {
        return pos < expression.length;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        String expression = "12.3 / 5 - 22";
        Tokenizer tokenizer = new Tokenizer(expression);
        while (tokenizer.hasMoreToken())
        {
            Lexeme nextToken = tokenizer.getToken();
            System.out.print("Type: " + nextToken.type + "\tLexeme: " + nextToken.token + "\n");
        }
    }
}